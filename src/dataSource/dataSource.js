import data from './data.json'

class DataSource { 
  get skills () {
    return data.skills
  }

  get interviewers () {
    return data.interviewers
  }
}

export default new DataSource()