import { useTheme } from '@mui/material/styles';
import useMediaQuery from '@mui/material/useMediaQuery';

export const useBreakpoints = (breakpoint, helper = 'down') => {
  const theme = useTheme();
  return useMediaQuery(theme.breakpoints[helper](breakpoint));
}