import { useState, useMemo } from 'react'
import dataSource from '../dataSource/dataSource'

const getFilteredData = (data, condition) => {
  return data.filter(condition)
}

export const useGetInterviewers = (filters) => {
  const [interviewers, setInterviewers] = useState([])

  useMemo(() => {
    if (filters) {
      let newData = dataSource.interviewers
      const { skill, level, isAvailable, isShadows } = filters

      if (skill) newData = getFilteredData(newData, (value) => new Set(value.skills).has(filters.skill) )

      if (level) newData = getFilteredData(newData, (value) => value.level === level)

      if (isAvailable) newData = getFilteredData(newData, (value) => value.availability.status === 'Confirmed')

      if (isShadows) newData = getFilteredData(newData, (value) => value.shadow_interviewer === isShadows)

      setInterviewers(newData)
    } else {
      setInterviewers([])
    }
  }, [filters])
  
  return interviewers
}