import React, { useState } from 'react'
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container'

import './App.scss'
import SearcherSection from './components/SearcherSection'
import InterviewersTable from './components/InterviewersTable';
import { useBreakpoints } from './customHooks/useBreakpoints';


function App() {
  const [filters, setFilters] = useState(null)
  const equalAndSmallerThanTablet = useBreakpoints('md', 'down')
  const containerStyle = !equalAndSmallerThanTablet ? { paddingTop: '20px', paddingLeft: '60px !important' } : {}
  return (
    <Container 
      maxWidth="xl"
      sx={containerStyle}
      className="app">
      <Typography
        className={['app__title', equalAndSmallerThanTablet && 'text-center'].filter(Boolean).join(' ')}
        variant="h2" 
        color="initial">Find a tech interviewer
      </Typography>
      <SearcherSection getFilters={setFilters}/>
      <InterviewersTable filters={filters} />
    </Container>
  );
}

export default App;
