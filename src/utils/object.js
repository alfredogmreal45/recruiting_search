/**
 * Extracts the object value by the key
 * @param {*} object Object Which will be extracted the specified value on key param 
 * @param {*} key Object property
 */
export const getNestedValue = (object, key) => {
  let value = object
  const keys = key.split('.')
  if (keys.length > 1) {
    keys.forEach(key => {
      value = value[key]
    })
    return value
  }
  return object[key]
}
