import React, { useState } from 'react';
import Button from '@mui/material/Button'
import Checkbox from '@mui/material/Checkbox';
import FormGroup from '@mui/material/FormGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import Grid from '@mui/material/Grid'
import useMediaQuery from '@mui/material/useMediaQuery';

import CustomAutocomplete from  './CustomAutocomplete'
import dataSource from '../dataSource/dataSource.js'
import { useBreakpoints } from '../customHooks/useBreakpoints'

function SearcherSection({ getFilters }) {
  const style = { 
    container: { pt: '20px !important' },
    ml: { ml: '30px' }
  }
  const levels = ['S1', 'S2', 'S3', 'S4']
  const [skill, setSkill] = useState(null)
  const [level, setLevel] = useState(null)
  const [isAvailable, setIsAvailable] = useState(false)
  const [isShadows, setIsShadows] = useState(false)
  const smDown = useBreakpoints('sm', 'down')
  const smallMobile = useMediaQuery('(max-width:535px)')

  const setSkillFilter = (selectedSkill) => {
    setSkill(selectedSkill)
  }

  const setLevelFilter = (selectedLevel) => {
    setLevel(selectedLevel)
  }

  const setAvailableFilter = (_, value) => {
    setIsAvailable(value)
  }

  const setShadowsFilter = (_, value) => {
    setIsShadows(value)
  }

  const emitFilters = () => {
    const filters = {
      skill,
      level,
      isAvailable,
      isShadows
    }

    getFilters(filters)
  }

  return (
    <Grid container sx={style.container}>
      <Grid item xs={12} sm={12} md={12} lg={6}>
        <Grid container justifyContent={smDown && 'center'}>
          <CustomAutocomplete 
            data={dataSource.skills} 
            label="Tech/Skill" 
            width="180" 
            onSelect={setSkillFilter}
          />
          <CustomAutocomplete
            data={levels}
            width="80"
            label="Level"
            sx={style.ml}
            onSelect={setLevelFilter}
          />
          <FormGroup sx={{...style.ml, ...(smallMobile && { mt: '20px' }) }}>
            <FormControlLabel 
              sx={{mt: '-10px'}} 
              control={<Checkbox checked={isAvailable} onChange={setAvailableFilter} />} 
              label="Solo disponibles" 
            />
            <FormControlLabel 
              control={<Checkbox checked={isShadows} onChange={setShadowsFilter} />} 
              label="Incluir Shadows" 
            />
          </FormGroup>
        </Grid>
      </Grid>
      <Grid item xs sm md lg sx={{ ...(smDown && { mt: '20px' })}}>
        <Grid container justifyContent={smDown ? 'center' : 'flex-end'} alignItems="center">
          <Button size="large" variant="contained" onClick={emitFilters}>Search!</Button>
        </Grid>
      </Grid>
    </Grid>
  );
}

export default SearcherSection;