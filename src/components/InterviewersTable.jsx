import React from 'react';
import TableCell from '@mui/material/TableCell'

import CustomTable from './CustomTable';
import { getNestedValue } from '../utils/object';
import { useGetInterviewers } from '../customHooks/useGetInterviewers'

const tableColumns = [ 
  { label: 'Nombre', props: {}, col: 'name'}, 
  { label: 'Level', props: {}, col: 'level'},
  { label: 'Skills/Techs', props: {}, col: 'skills', type: 'list'}, 
  { label: 'Notas', props: {}, col: 'notes'},
  { label: 'Slot1', props: {}, col: 'slots', order: 0 }, 
  { label: 'Slot2', props: {}, col: 'slots', order: 1 }, 
  { label: 'Slot3', props: {}, col: 'slots', order: 2 }, 
  { label: 'Slot4', props: {}, col: 'slots', order: 3 }, 
  { label: 'Slot5', props: {}, col: 'slots', order: 4 }, 
  { label: 'Status', props: {}, col: 'availability.status'}, 
  { label: 'Entrevista Solo', props: {}, col: 'shadow_interviewer'}, 
]

const renderValue = (value, type, order, highlightMatch) => {
  if (order !== undefined) return value[order]

  if (type && type === 'list') {
    return value.map((item, i) => 
    <span 
      key={i}
      className={(item === highlightMatch) ? 'font-weight-bold' : ''}>
        {item}{i < value.length - 1 && ', '}
    </span>)
  }

  return value
}

const renderDate = (stringDate) => {
  if (stringDate) {
    const date = new Date(stringDate)
    const day = date.toLocaleDateString('es-MX', {day: 'numeric' })
    const month = date.toLocaleDateString('es-MX', {month: 'short' })
    return `(${day}/${month})`
  }
}

function InterviewersTable({ filters }) {
  const interviewers = useGetInterviewers(filters)
  
  const renderRow = (row) => 
    tableColumns.map(({ col, type, order }, key) => {
      let rowValue = renderValue(getNestedValue(row, col), type, order, filters.skill)

      if (col === 'shadow_interviewer') rowValue = rowValue ? 'No' : 'Si'
      
      return <TableCell 
          key={key}>
            {rowValue}
            <br/>
            { col === 'availability.status' && renderDate(row.availability.updated)}
        </TableCell>
      }
    )

  return (
    <CustomTable
      customClass="interviewers-table"
      title="Tech interviewers"
      columns={tableColumns} 
      style={{ mt: '40px' }}
      rows={interviewers}
      renderRow={renderRow}
    />
  );
}

export default InterviewersTable;