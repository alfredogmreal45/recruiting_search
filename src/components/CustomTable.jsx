import React from 'react';
import Box from '@mui/material/Box'
import { blue } from '@mui/material/colors';
import Paper from '@mui/material/Paper';
import { styled } from '@mui/material/styles';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell, { tableCellClasses } from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Typography from '@mui/material/Typography'

import { useBreakpoints } from '../customHooks/useBreakpoints';

const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    backgroundColor: blue[100]
  }
}));

const StyledTableContainer = styled(TableContainer)(({ theme }) => ({
  maxHeight: 600,
  scrollbarWidth: 'none',
  msOverflowStyle: 'none',
  [`&::-webkit-scrollbar`]: {
    display: 'none'
  }
}))

function CustomTable({ columns, style, title = '', rows = [], renderRow, customClass }) {
  const smDown = useBreakpoints('sm', 'down')

  const countRecords = (key) => (
    <Typography
      key={key}
      variant="h5" 
      color="initial" 
      className={['text-center', !smDown && 'font-weight-bold'].filter(Boolean).join(' ')} 
      sx={{...(smDown && {mb: 2 })}}>
      Hay {rows.length} resultado{rows.length !== 1 && 's'}
    </Typography>
  )

  const tableTitle = (key) => (
    <Typography
      key={key} 
      variant={smDown ? 'h4' : 'h5'} 
      color="initial"
      className={smDown ? 'text-center font-weight-bold' : ''} 
      sx={{ mb: '20px', ...(smDown && {mt: '20px'})}} 
      >
        {title}
    </Typography>
  )

  const titles = [countRecords, tableTitle]

  return (
    <Box sx={style} className={customClass}>
      {smDown ? titles.reverse().map((item, i) => item(i)) : titles.map((title, i) => title(i))}
      <StyledTableContainer component={Paper}>
        <Table sx={{ minWidth: 650 }} stickyHeader aria-label="custom table">
          <TableHead>
            <TableRow>
              {columns.map(col => 
                <StyledTableCell 
                  key={col.label} 
                  {...col.props || {}}> 
                    {col.label} 
                </StyledTableCell>
              )}
            </TableRow>
          </TableHead>
          <TableBody>
              {renderRow && rows.map((row, i) => <TableRow key={i}>{renderRow(row)}</TableRow>)}
          </TableBody>
        </Table>
      </StyledTableContainer>
    </Box>
  );
}

export default CustomTable;