import React from 'react';
import TextField from '@mui/material/TextField';
import Autocomplete from '@mui/material/Autocomplete';

function CustomAutocomplete({ data, label, width, onSelect, sx = {} }) {
  return (
    <Autocomplete
      onChange={(_, newValue) => {
        onSelect && onSelect(newValue)
      }}
      freeSolo
      disablePortal
      options={data}
      sx={{ width: +width || 300, ...sx }}
      renderInput={(params) => <TextField {...params} label={label} />}
    />
  );
}

export default CustomAutocomplete;